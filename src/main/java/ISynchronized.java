import model.World;

/**
 * Классы, поддерживающие этот интерфейс, синхронизованы по монитору State
 */
interface ISynchronized {

    /**
     * Устанавливает монитор, по которому осуществялется синхронизация
     * @param moveMonitor монитор для синхронизации
     */
    void setMoveMonitor(MoveMonitor moveMonitor);

    /**
     * Устанавливает общий для всех контроллеров этап игры
     * @param gameStage содержит этап игры
     */
    void setGameStage(GameStage gameStage);

    /**
     * Рассчитывает необходимость хода этим контроллером
     * @return необходимость хода
     */
    double necessaryOfMove(World world, double resourcesToMakeMove);

    /**
     * У снихронизованных контроллеров есть жизненный цикл,
     * после выполнения своей задачи контролер счетается завершенным
     */
    boolean isFinished();
}
