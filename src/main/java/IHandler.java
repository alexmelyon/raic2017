import model.World;

/**
 * Классы, поддерживающие этот интерфейс, являются контроллерами
 */

interface IHandler {

    /**
     * Метод, который выполняет основное действие контроллера
     * @param world игровой мир
     * @param move игровое действие
     * @param resourcesToMakeMove кол-во ресурсов, для совершения хода
     */
    void doAction(World world, MyMove move, double resourcesToMakeMove);
}
