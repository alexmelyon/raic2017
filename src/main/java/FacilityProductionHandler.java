import model.FacilityType;
import model.VehicleType;
import model.World;

class FacilityProductionHandler extends AbstractHandler {

    // NECESSARY_COEFFICIENT - коэффициент необходимости хода
    private static final double NECESSARY_COEFFICIENT = 2.0;
    // SIMULATE_PRODUCTION_TICKS - количество тиков, в течении которых будут производиться юниты, для моделирования
    private static final int SIMULATE_PRODUCTION_TICKS = 900;

    // необходимый производимый тип техники и номер актуального сооружения
    private VehicleType productionType = VehicleType.TANK;
    private int facilityId = 0;

    // юниты, которые будут произведены на сооружениях
    private int[] opAdditionalUnits;
    private int[] myAdditionalUnits;

    FacilityProductionHandler(Army army) {
        this.army = army;
    }

    @Override
    public void doAction(World world, MyMove move, double resourcesToMakeMove) {

        this.world = world;
        this.move = move;
        this.resourcesToMakeMove = resourcesToMakeMove;

        if (resourcesToMakeMove > 1) {
            move.setupVehicleProduction(facilityId, productionType);
            System.out.println("[TICK INDEX = " + world.getTickIndex() + "] " + productionType + " PRODUCTION STARTED");
        }

    }

    @Override
    public double necessaryOfMove(World world, double resourcesToMakeMove) {
        updateAdditionalUnits(world);
        updateProductionType();
        return isNewVehiclesFactorySeized(world) || isTimeToReplaceVehicle(world) ? NECESSARY_COEFFICIENT : 0;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    private boolean isNewVehiclesFactorySeized(World world) {

        for (MyFacility facility : army.getFacilities()) {
            if (facility.getType() == FacilityType.VEHICLE_FACTORY &&
                    facility.getPlayerId() == world.getMyPlayer().getId() &&
                    facility.getVehicleType() == null) {

                facilityId = facility.getUniqueId();
                return true;
            }
        }

        return false;
    }

    private boolean isTimeToReplaceVehicle(World world) {

        for (MyFacility facility : army.getFacilities()) {
            if (facility.getType() == FacilityType.VEHICLE_FACTORY &&
                    facility.getPlayerId() == world.getMyPlayer().getId() &&
                    facility.getVehicleType() != productionType &&
                    army.getMyBattalions().stream().noneMatch(b -> b.getUniqueId() == -facility.getUniqueId())) {

                facilityId = facility.getUniqueId();
                return true;
            }
        }

        return false;
    }

    private void updateAdditionalUnits(World world) {

        opAdditionalUnits = new int[VehicleType.values().length];
        myAdditionalUnits = new int[VehicleType.values().length];

        for (MyFacility facility : army.getFacilities()) {
            if (facility.getType() == FacilityType.VEHICLE_FACTORY &&
                    facility.getVehicleType() != null) {

                VehicleType type = facility.getVehicleType();

                if (facility.getPlayerId() == world.getOpponentPlayer().getId()) {
                    opAdditionalUnits[type.ordinal()] += SIMULATE_PRODUCTION_TICKS / MyGame.productionCostOfType(type);
                } else if (facility.getPlayerId() == world.getMyPlayer().getId()) {
                    myAdditionalUnits[type.ordinal()] += SIMULATE_PRODUCTION_TICKS / MyGame.productionCostOfType(type);
                }
            }
        }
    }
    private void updateProductionType() {

        double profit = Double.NEGATIVE_INFINITY;
        Battalion op = abstractOpBattalion();

        for (VehicleType type : VehicleType.values()) {

            Battalion me = abstractMeBattalionWithAdditionalType(type);
            double currentProfit = calcPowerCoefficient(me, op);

            if (profit < currentProfit) {
                profit = currentProfit;
                productionType = type;
            }
        }
    }

    private Battalion abstractOpBattalion() {

        int opf = army.opUnitsCountOf(VehicleType.FIGHTER) + opAdditionalUnits[VehicleType.FIGHTER.ordinal()];
        int oph = army.opUnitsCountOf(VehicleType.HELICOPTER) + opAdditionalUnits[VehicleType.HELICOPTER.ordinal()];
        int opa = army.opUnitsCountOf(VehicleType.ARRV) + opAdditionalUnits[VehicleType.ARRV.ordinal()];
        int opi = army.opUnitsCountOf(VehicleType.IFV) + opAdditionalUnits[VehicleType.IFV.ordinal()];
        int opt = army.opUnitsCountOf(VehicleType.TANK) + opAdditionalUnits[VehicleType.TANK.ordinal()];

        Battalion op = new Battalion();
        op.setAbstract(opf, oph, opa, opi, opt);

        return op;
    }
    private Battalion abstractMeBattalionWithAdditionalType(VehicleType type) {

        int[] myX = new int[VehicleType.values().length];
        myX[VehicleType.FIGHTER.ordinal()] = army.myUnitsCountOf(VehicleType.FIGHTER) + myAdditionalUnits[VehicleType.FIGHTER.ordinal()];
        myX[VehicleType.HELICOPTER.ordinal()] = army.myUnitsCountOf(VehicleType.HELICOPTER) + myAdditionalUnits[VehicleType.HELICOPTER.ordinal()];
        myX[VehicleType.ARRV.ordinal()] = army.myUnitsCountOf(VehicleType.ARRV) + myAdditionalUnits[VehicleType.ARRV.ordinal()];
        myX[VehicleType.IFV.ordinal()] = army.myUnitsCountOf(VehicleType.IFV) + myAdditionalUnits[VehicleType.IFV.ordinal()];
        myX[VehicleType.TANK.ordinal()] = army.myUnitsCountOf(VehicleType.TANK) + myAdditionalUnits[VehicleType.TANK.ordinal()];

        myX[type.ordinal()] += SIMULATE_PRODUCTION_TICKS / MyGame.productionCostOfType(type);

        Battalion me = new Battalion();
        me.setAbstract(
                myX[VehicleType.FIGHTER.ordinal()],
                myX[VehicleType.HELICOPTER.ordinal()],
                myX[VehicleType.ARRV.ordinal()],
                myX[VehicleType.IFV.ordinal()],
                myX[VehicleType.TANK.ordinal()]);

        return me;
    }

    private double calcPowerCoefficient(Battalion me, Battalion op) {

        double myHealth = me.getTotalDurability();
        double opHealth = op.getTotalDurability();
        double myDamage = calcDamage(me, op);
        double opDamage = calcDamage(op, me);

        int nIter = 0;
        int maxIter = 10;
        double myCurrentHealth = myHealth;
        double opCurrentHealth = opHealth;
        double myCurrentDamage = myDamage;
        double opCurrentDamage = opDamage;

        while (myCurrentHealth > 0 && opCurrentHealth > 0 && nIter++ < maxIter) {
            myCurrentHealth -= opCurrentDamage;
            opCurrentHealth -= myCurrentDamage;
            myCurrentDamage = myDamage * myCurrentHealth / myHealth;
            opCurrentDamage = opDamage * opCurrentHealth / opHealth;
        }

        double myRemaining = me.getCount() * Math.max(0, myCurrentHealth / myHealth);
        double opRemaining = op.getCount() * Math.max(0, opCurrentHealth / opHealth);
        double myLost = me.getCount() - myRemaining;
        double opLost = op.getCount() - opRemaining;

        return opLost - myLost;
    }
    private double calcDamage(Battalion attacker, Battalion defender) {

        int nf = attacker.getCountOfType(VehicleType.FIGHTER);
        int nh = attacker.getCountOfType(VehicleType.HELICOPTER);
        int na = attacker.getCountOfType(VehicleType.ARRV);
        int ni = attacker.getCountOfType(VehicleType.IFV);
        int nt = attacker.getCountOfType(VehicleType.TANK);
        int n0 = attacker.getCount();
        int nA = nf + nh;
        int nG = n0 - nA;
        double pA = ((double) nA) / n0;
        double pG = ((double) nG) / n0;

        int mf = defender.getCountOfType(VehicleType.FIGHTER);
        int mh = defender.getCountOfType(VehicleType.HELICOPTER);
        int ma = defender.getCountOfType(VehicleType.ARRV);
        int mi = defender.getCountOfType(VehicleType.IFV);
        int mt = defender.getCountOfType(VehicleType.TANK);
        int m0 = defender.getCount();
        int mA = mf + mh;
        int mG = m0 - mA;
        double qA = ((double) mA) / m0;
        double qG = ((double) mG) / m0;

        double res = 0;

        // pA & qA
        if (pA > 0 && qA > 0) {
            double avgDefAA = (mf * MyGame.aerialDefenceOfType(VehicleType.FIGHTER)
                    + mh * MyGame.aerialDefenceOfType(VehicleType.HELICOPTER))
                    / mA;
            double dmgfA = Math.max(0, nf * (MyGame.aerialDamageOfType(VehicleType.FIGHTER) - avgDefAA));
            double dmghA = Math.max(0, nh * (MyGame.aerialDamageOfType(VehicleType.HELICOPTER) - avgDefAA));
            res += pA * qA * (dmgfA + dmghA);
        }

        // pA & qG
        if (pA > 0 && qG > 0) {
            double avgDefAG = (mt * MyGame.aerialDefenceOfType(VehicleType.TANK)
                    + mi * MyGame.aerialDefenceOfType(VehicleType.IFV)
                    + ma * MyGame.aerialDefenceOfType(VehicleType.ARRV)
                    + ma * MyGame.instance.getArrvRepairSpeed() * MyGame.instance.getTankAttackCooldownTicks())
                    / mG;
            double dmgfG = Math.max(0, nf * (MyGame.groundDamageOfType(VehicleType.FIGHTER) - avgDefAG));
            double dmghG = Math.max(0, nh * (MyGame.groundDamageOfType(VehicleType.HELICOPTER) - avgDefAG));
            res += pA * qG * (dmgfG + dmghG);
        }

        // pG & qA
        if (pG > 0 && qA > 0) {
            double avgDefGA = (mf * MyGame.groundDefenceOfType(VehicleType.FIGHTER)
                    + mh * MyGame.groundDefenceOfType(VehicleType.HELICOPTER))
                    / mA;
            double dmgtA = Math.max(0, nt * (MyGame.aerialDamageOfType(VehicleType.TANK) - avgDefGA));
            double dmgiA = Math.max(0, ni * (MyGame.aerialDamageOfType(VehicleType.IFV) - avgDefGA));
            res += pG * qA * (dmgtA + dmgiA);
        }

        // pG & qG
        if (pG > 0 && qG > 0) {
            double avgDefGG = (mt * MyGame.groundDefenceOfType(VehicleType.TANK)
                    + mi * MyGame.groundDefenceOfType(VehicleType.IFV)
                    + ma * MyGame.groundDefenceOfType(VehicleType.ARRV)
                    + ma * MyGame.instance.getArrvRepairSpeed() * MyGame.instance.getTankAttackCooldownTicks())
                    / mG;
            double dmgtA = Math.max(0, nt * (MyGame.groundDamageOfType(VehicleType.TANK) - avgDefGG));
            double dmgiA = Math.max(0, ni * (MyGame.groundDamageOfType(VehicleType.IFV) - avgDefGG));
            res += pG * qG * (dmgtA + dmgiA);
        }

        return res;
    }
}
