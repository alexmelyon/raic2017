import model.ActionType;
import model.Move;
import model.VehicleType;
import model.World;

class MyMove {

    private World world;
    private Move instance;

    MyMove(Move instance, World world) {

        this.instance = instance;
        this.world = world;
    }

    // SELECT
    void selectAll(VehicleType type) {

        select(new Rectangle(0, 0, world.getWidth(), world.getHeight()));
        instance.setVehicleType(type);
    }
    void select(Rectangle rectangle, VehicleType type) {

        instance.setVehicleType(type);
        select(rectangle);
    }
    void select(Rectangle rectangle) {

        instance.setAction(ActionType.CLEAR_AND_SELECT);
        instance.setLeft(rectangle.getLeft());
        instance.setTop(rectangle.getTop());
        instance.setRight(rectangle.getRight());
        instance.setBottom(rectangle.getBottom());
    }
    void select(Battalion battalion) {

        instance.setAction(ActionType.CLEAR_AND_SELECT);
        instance.setGroup(battalion.getUniqueId());
    }

    // BIND
    void bind(Battalion battalion) {

        instance.setAction(ActionType.ASSIGN);
        instance.setGroup(battalion.getUniqueId());
    }

    // MOVE
    void move(Battalion battalion, Vector2D direction, double maxSpeed) {

        move(battalion, direction);
        instance.setMaxSpeed(maxSpeed);
    }
    void move(Battalion battalion, Vector2D direction) {

        battalion.setState(Battalion.State.MOVE);
        move(direction);
    }
    void move(Vector2D direction) {

        instance.setAction(ActionType.MOVE);
        instance.setX(direction.getX());
        instance.setY(direction.getY());
    }

    // SCALE
    void scale(Battalion battalion, Pos from, double scaleFactor) {

        battalion.setState(Battalion.State.SCALE);
        instance.setAction(ActionType.SCALE);
        instance.setFactor(scaleFactor);
        instance.setX(from.x);
        instance.setY(from.y);
    }

    // ROTATE
    void rotate(Battalion battalion, Pos center, double angle) {

        battalion.setState(Battalion.State.ROTATE);
        instance.setAction(ActionType.ROTATE);
        instance.setX(center.x);
        instance.setY(center.y);
        instance.setAngle(angle);
    }

    // NUCLEAR STRIKE
    void nuclearStrike(Pos target, int vehicleId) {

        instance.setAction(ActionType.TACTICAL_NUCLEAR_STRIKE);
        instance.setX(target.x);
        instance.setY(target.y);
        instance.setVehicleId(vehicleId);
    }

    void setupVehicleProduction(int facilityId, VehicleType productionType) {

        instance.setAction(ActionType.SETUP_VEHICLE_PRODUCTION);
        instance.setFacilityId(facilityId);
        instance.setVehicleType(productionType);
    }
}
