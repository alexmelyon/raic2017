import model.World;

import javax.swing.*;
import java.awt.*;

public class ZWindow extends JFrame implements Runnable {

    private static final String WINDOW_NAME = "Raic 2017. Leos Visualizer.";
    private ZCanvasPane rootPane;

    ZWindow() {
        setTitle(WINDOW_NAME);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    void start() {

        rootPane = new ZCanvasPane();
        rootPane.setDoubleBuffered(false);
        setSize(1030, 1054);
        setLayout(new BorderLayout());
        add("Center", rootPane);
        setVisible(true);
    }

    @Override
    public void run() {
        start();
    }

    void update(World world, Army army) {
        rootPane.update(world, army);
        validate();
        getContentPane().repaint();
    }
}