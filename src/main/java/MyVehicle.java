import model.VehicleType;

class MyVehicle {

    private int uniqueId;
    private int playerId;
    private int battalionId;
    private double x;
    private double y;
    private VehicleType type;
    private int durability;
    private int maxDurability;
    private boolean isSelected;
    private boolean isCoordinatesUpdated;

    MyVehicle(int uniqueId, int playerId, double x, double y, VehicleType type, int maxDurability) {
        this.uniqueId = uniqueId;
        this.playerId = playerId;
        this.x = x;
        this.y = y;
        this.type = type;
        this.maxDurability = maxDurability;
        this.durability = maxDurability;
    }

    int getUniqueId() {
        return uniqueId;
    }

    int getPlayerId() {
        return playerId;
    }

    int getBattalionId() {
        return battalionId;
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }

    VehicleType getType() {
        return type;
    }

    int getMaxDurability() {
        return maxDurability;
    }

    int getDurability() {
        return durability;
    }

    boolean isSelected() {
        return isSelected;
    }

    boolean isCoordinatesUpdated() {
        return isCoordinatesUpdated;
    }

    void setBattalionId(int battalionId) {
        this.battalionId = battalionId;
    }

    void setX(double x) {
        this.x = x;
    }

    void setY(double y) {
        this.y = y;
    }

    void setDurability(int durability) {
        this.durability = durability;
    }

    void setSelected(boolean selected) {
        isSelected = selected;
    }

    void setCoordinatesUpdated(boolean updated) {
        isCoordinatesUpdated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyVehicle vehicle = (MyVehicle) o;

        return uniqueId == vehicle.uniqueId;
    }

    @Override
    public int hashCode() {
        return uniqueId;
    }
}
