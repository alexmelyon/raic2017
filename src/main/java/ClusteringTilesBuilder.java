import model.World;

import java.util.List;
import java.util.function.Predicate;

class ClusteringTilesBuilder {

     ClusteringTiles build(World world, List<MyVehicle> vehicles, Predicate<MyVehicle> filter) {

         ClusteringTiles tiles = new ClusteringTiles(world, vehicles, filter);

         tiles.addVehicles();
         tiles.checkNeighbors();
         tiles.combineGroups();

         return tiles;
     }
}
