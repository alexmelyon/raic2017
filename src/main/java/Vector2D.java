class Vector2D {

    private double x;
    private double y;

    Vector2D() {
    }

    Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    Vector2D(Pos from, Pos to) {
        x = to.x - from.x;
        y = to.y - from.y;
    }

    Vector2D norm() {
        double length = Math.sqrt(x * x + y * y);
        return new Vector2D(x / length, y / length);
    }

    Vector2D norm(double c) {
        double length = Math.sqrt(x * x + y * y);
        return new Vector2D(c * x / length, c * y / length);
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }

    double dotprod(Vector2D v) {
        return x * v.x + y * v.y;
    }

    double length() {
        return Math.sqrt(x * x + y * y);
    }

    double xMult(Vector2D v) {
        return x * v.y - y * v.x;
    }

    void setX(double x) {
        this.x = x;
    }

    void setY(double y) {
        this.y = y;
    }

    Vector2D mult(double c) {
        return new Vector2D(c * x, c * y);
    }
}
