import model.*;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

class MainHandler implements IHandler {

    // MIN_NECESSARY_OF_MOVE - минимальное ограничение необходимости на ход
    private static final double MIN_NECESSARY_OF_MOVE = 1;

    // Внешние параметры
    private World world;
    private MyGame game;
    private MyMove move;
    private double resourcesToMakeMove;

    // Синхронизация контроллеров
    private GameStage gameStage;
    private MoveMonitor moveMonitor;

    // Контроллеры
    private Map<HandlerType, AbstractHandler> handlers;


    MainHandler(Army army, long randomSeed) {

        gameStage = new GameStage();
        moveMonitor = new MoveMonitor();

        handlers = new EnumMap<>(HandlerType.class);
        handlers.put(HandlerType.FORMATION, new FormationHandler(army));
        handlers.put(HandlerType.BATTALIONS, new BattalionsHandler(army, randomSeed));
        handlers.put(HandlerType.NUCLEAR_STRIKE, new NuclearStrikeHandler(army));
        handlers.put(HandlerType.FACILITY_PRODUCTION, new FacilityProductionHandler(army));

        for (Map.Entry<HandlerType, AbstractHandler> handlerEntry : handlers.entrySet()) {
            handlerEntry.getValue().setGameStage(gameStage);
            handlerEntry.getValue().setMoveMonitor(moveMonitor);
        }
    }

    @Override
    public void doAction(World world, MyMove move, double resourcesToMakeMove) {

        this.world = world;
        this.move = move;
        this.resourcesToMakeMove = resourcesToMakeMove;

        updateGameStage();

        switch (gameStage.getState()) {

            case EARLY_GAME:
                synchronizedEarlySection();
                break;

            case MIDDLE_GAME:
                synchronizedMiddleSection();
                break;
        }
    }

    private void updateGameStage() {

        if (!handlers.get(HandlerType.FORMATION).isFinished()) {
            gameStage.setState(GameStage.State.EARLY_GAME);
        } else {
            gameStage.setState(GameStage.State.MIDDLE_GAME);
        }
    }

    private void synchronizedEarlySection() {

        switch (moveMonitor.getState()) {

            case FORMATION: {
                handlers.get(HandlerType.FORMATION)
                        .doAction(world, move, resourcesToMakeMove);
                break;
            }

            case BATTALION: {
                handlers.get(HandlerType.BATTALIONS)
                        .doAction(world, move, resourcesToMakeMove);
                break;
            }

            case FREE: {
                doMaxNecessaryMove();
                break;
            }
        }
    }

    private void synchronizedMiddleSection() {

        switch (moveMonitor.getState()) {

            case BATTALION: {
                handlers.get(HandlerType.BATTALIONS).doAction(world, move, resourcesToMakeMove);
                break;
            }

            case FREE:{
                doMaxNecessaryMove();
                break;
            }
        }
    }

    private void doMaxNecessaryMove() {

        HandlerType maxNecessaryKey = null;
        double maxNecessaryOfMove = Double.NEGATIVE_INFINITY;

        for (Map.Entry<HandlerType, AbstractHandler> handlerEntry : handlers.entrySet()) {
            double necessaryOfMove = handlerEntry.getValue().necessaryOfMove(world, resourcesToMakeMove);
            if (necessaryOfMove > maxNecessaryOfMove) {
                maxNecessaryKey = handlerEntry.getKey();
                maxNecessaryOfMove = necessaryOfMove;
            }
        }
        //System.out.println("[TICK INDEX = " + world.getTickIndex() + "] " + maxNecessaryKey + ": " + maxNecessaryOfMove);
        //System.out.println();

        if (maxNecessaryOfMove >= MIN_NECESSARY_OF_MOVE) {
            handlers.get(maxNecessaryKey).doAction(world, move, resourcesToMakeMove);
        }
        /*
        else if (resourcesToMakeMove > 1){
            System.out.println("[TICK INDEX = " + world.getTickIndex() + "] No need to move $resource = " + resourcesToMakeMove );
        }
        */
    }


    enum HandlerType {

        FORMATION,
        BATTALIONS,
        NUCLEAR_STRIKE,
        FACILITY_PRODUCTION
    }
}
