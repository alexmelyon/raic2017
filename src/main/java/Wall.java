/**
 * Перечисление границы карты, которые являются отталкивающими потенциалами
 */
enum Wall implements ISource {

    LEFT {
        @Override
        public double calcPotential(long playerId, Battalion battalion, Pos target){
            double dist = Math.abs(target.x);
            return -Math.exp(-dist / battalion.getMaxRadius() / WALL_REPEL_COEFFICIENT);
        }
    },

    RIGHT{
        @Override
        public double calcPotential(long playerId, Battalion battalion, Pos target){
            double dist = Math.abs(GAME_WORLD_WIDTH - target.x);
            return -Math.exp(-dist / battalion.getMaxRadius() / WALL_REPEL_COEFFICIENT);
        }
    },

    TOP{
        @Override
        public double calcPotential(long playerId, Battalion battalion, Pos target){
            double dist = Math.abs(target.y);
            return -Math.exp(-dist / battalion.getMaxRadius() / WALL_REPEL_COEFFICIENT);
        }
    },

    BOTTOM{
        @Override
        public double calcPotential(long playerId, Battalion battalion, Pos target){
            double dist = Math.abs(GAME_WORLD_HEIGHT - target.y);
            return -Math.exp(-dist / battalion.getMaxRadius() / WALL_REPEL_COEFFICIENT);
        }
    },

    CENTER{
        @Override
        public double calcPotential(long playerId, Battalion battalion, Pos target) {
            Pos center = new Pos(GAME_WORLD_WIDTH / 2, GAME_WORLD_HEIGHT / 2);
            double dist = Math.abs(center.distanceTo(target));
            return dist > DIST_FROM_CENTER_WITHOUT_PENALTY ?
                    -ANTICENTER_REPEL_COEFFICIENT * (DIST_FROM_CENTER_WITHOUT_PENALTY - dist) * (DIST_FROM_CENTER_WITHOUT_PENALTY - dist)
                    : 0;
        }
    };

    private static final double GAME_WORLD_WIDTH = 1024;
    private static final double GAME_WORLD_HEIGHT = 1024;
    private static final double DIST_FROM_CENTER_WITHOUT_PENALTY = 450;
    private static final double ANTICENTER_REPEL_COEFFICIENT = 0.0000003;
    private static final double WALL_REPEL_COEFFICIENT = 0.16;
}


