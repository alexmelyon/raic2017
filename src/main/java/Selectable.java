/**
 * Классы поддерживающщие этот объект можно выделить или считать препятствием
 */
interface Selectable {

    Pos getCenter();
    double getRadius();
    double getLeft();
    double getTop();
    double getRight();
    double getBottom();
}
