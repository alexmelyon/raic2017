import model.World;

import javax.swing.*;
import java.awt.*;

class ZCanvasPane extends JPanel {

    private static final int CANVAS_WIDTH = 1024;
    private static final int CANVAS_HEIGHT = 1024;
    private static final int NUMBER_OF_ROWS = 256;
    private static final int NUMBER_OF_COLS = 256;

    private static final Color myColor = new Color(0, 255, 0);
    private static final Color opColor = new Color(150, 40, 140);

    private World world;
    private Army army;
    private BattalionsHandler battalionsHandler;
    private double potential0;


    void update(World world, Army army) {

        this.world = world;
        this.army = army;
    }

    @Override
    public void paint(Graphics g) {

        if (army == null || world == null || army.getCurrent() == null ) {
            return;
        }

        battalionsHandler = new BattalionsHandler(army, 0);
        battalionsHandler.setWorld(world);
        battalionsHandler.calcPowerCoefficients();
        battalionsHandler.calcFacilitiesCoefficients();

        drawPotential(g);
        drawFacilities(g);
        drawBattalions(g);
    }

    private void drawPotential(Graphics g) {

        potential0 = (float) calcScaledPotentialFor(army.getCurrent().getCenter());

        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            for (int j = 0; j < NUMBER_OF_COLS; j++) {
                int stepX = CANVAS_WIDTH / NUMBER_OF_COLS;
                int stepY = CANVAS_HEIGHT / NUMBER_OF_ROWS;

                g.setColor(calcColorFor(i, j));
                g.fillRect(stepX * j, stepY * i, stepX, stepY);
            }
        }
    }

    private void drawFacilities(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));

        for (MyFacility facility : army.getFacilities()) {
            int side = (int) facility.getSideSize();
            g.setColor(facility.getPlayerId() == 1 ?  myColor : facility.getPlayerId() == 2 ? opColor : Color.BLACK);
            g.drawRect((int) facility.getLeft(), (int) facility.getTop(), side, side);
        }
    }
    private void drawBattalions(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        for (Battalion battalion : army.getMyBattalions()) {
            g2.setStroke(new BasicStroke(battalion.getUniqueId() == army.getCurrent().getUniqueId() ? 5 : 2));
            drawBattalion(g, battalion, myColor);
        }

        g.setColor(Color.GRAY);
        g2.setStroke(new BasicStroke(2));
        g.drawLine((int) army.getCurrent().getCenter().x, (int) army.getCurrent().getCenter().y,
                (int) army.getCurrent().getTarget().x, (int) army.getCurrent().getTarget().y);

        g.setColor(opColor);
        g2.setStroke(new BasicStroke(2));

        for (Battalion battalion : army.getOpBattalions()) {
            drawBattalion(g, battalion, opColor);
        }
    }
    private void drawBattalion(Graphics g, Battalion battalion, Color color) {

        Pos c = battalion.getCenter();
        Pos r1 = battalion.getCenter().shift(battalion.getCore().getV1().norm(battalion.getCore().getR1()));
        Pos r2 = battalion.getCenter().shift(battalion.getCore().getV2().norm(battalion.getCore().getR2()));

        int delta = (int) battalion.getMaxRadius();
        int inr = 2 + (int) Math.sqrt(battalion.getCount() * Battalion.INNER_REGROUP_COEFFICIENT);
        int outr = 2 + (int) Math.sqrt(battalion.getCount() * Battalion.OUTER_REGROUP_COEFFICIENT);
        int curr = 2 + (int) Math.sqrt(battalion.getCore().getR1() * battalion.getCore().getR2());

        g.setColor(color);
        //g.drawOval((int) c.x - delta, (int) c.y - delta,2 * delta, 2 * delta);
        //g.drawOval((int) c.x - inr, (int) c.y - inr,2 * inr, 2 * inr);
        //g.drawOval((int) c.x - outr, (int) c.y - outr,2 * outr, 2 * outr);
        g.drawOval((int) c.x - curr, (int) c.y - curr,2 * curr, 2 * curr);
        g.drawLine((int) c.x, (int) c.y, (int) r2.x, (int) r2.y);
        g.setColor(Color.DARK_GRAY);
        g.drawLine((int) c.x, (int) c.y, (int) r1.x, (int) r1.y);

    }

    // ВЫЧИСЛЕНИЕ ПОТЕНЦИАЛОВ
    private Color calcColorFor(int row, int col) {

        Color color;
        int stepX = CANVAS_WIDTH / NUMBER_OF_COLS;
        int stepY = CANVAS_HEIGHT / NUMBER_OF_ROWS;
        float potential = (float) calcScaledPotentialFor(new Pos(stepX * col + stepX / 2, stepY * row + stepY / 2));
        potential -= potential0;
        float intensity = 0.5f;

        if (Math.abs(potential) > 2) {
            color = Color.BLACK;
        } else if (potential > 0) {
            //color = new Color(192, 192, 255);
            color = new Color(1 - intensity * potential, 1 - intensity * potential, 1.0f);
        } else {
            //color = new Color(255, 192, 192);
            color = new Color(1.0f, 1 + intensity * potential, 1 + intensity * potential);
        }

        return color;
    }
    private double calcScaledPotentialFor(Pos target) {

        double potential =
           //     + getAllyPotential(target)
            //            + getEnemyPotential(target)
                        + getFacilitiesPotential(target)
             //           + getWallsPotential(target)
                ;

        double sign = potential > 0 ? 1 : -1;
        potential = sign * Math.max(0, 1 + Math.log10(Math.abs(potential)) / 5);

        return potential;
    }
    private double getAllyPotential(Pos target) {

        double potential = 0;

        for (Battalion battalion : army.getMyBattalions()) {
            potential += battalion.calcPotential(1, army.getCurrent(), target);
        }

        return potential;
    }
    private double getEnemyPotential(Pos target) {

        double potential = 0;

        for (Battalion battalion : army.getOpBattalions()) {
            potential += battalionsHandler.calcEnemyPotential(army.getCurrent(), battalion, target);
        }

        return potential;
    }
    private double getFacilitiesPotential(Pos target) {

        double potential = 0;

        for (MyFacility facility : army.getFacilities()) {
            //potential += facility.calcPotential(world.getMyPlayer().getId(), army.getCurrent(), target);
            potential += battalionsHandler.calcFacilityPotential(army.getCurrent(), facility, target);
        }

        return potential;
    }
    private double getWallsPotential(Pos target) {

        double potential = 0;

        for (Wall wall : army.getWalls()) {
            potential += wall.calcPotential(world.getMyPlayer().getId(), army.getCurrent(), target);
        }

        return potential;
    }
}
