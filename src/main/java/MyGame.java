import model.*;

class MyGame {

    static Game instance;

    static void setGame(Game instance) {
        MyGame.instance = instance;
    }

    static boolean isFly(VehicleType type) {
        return type == VehicleType.FIGHTER ||
                type == VehicleType.HELICOPTER;
    }

    static double damageMinusDefenceOf(VehicleType attacker, VehicleType defender) {
        return Math.max(0, damageOfType(attacker, defender) - defenceOfType(defender, attacker));
    }

    // DAMAGE
    static double damageOfType(VehicleType type, VehicleType opponentType) {
        return damageOfType(type, isFly(opponentType));
    }
    static double damageOfType(VehicleType type, boolean isOpponentFly) {
        if (isOpponentFly) {
            return aerialDamageOfType(type);
        } else {
            return groundDamageOfType(type);
        }
    }
    static double aerialDamageOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return 0;
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterAerialDamage();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterAerialDamage();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvAerialDamage();
        } else if (type == VehicleType.TANK) {
            return instance.getTankAerialDamage();
        } else {
            throw new UnsupportedOperationException();
        }
    }
    static double groundDamageOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return 0;
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterGroundDamage();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterGroundDamage();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvGroundDamage();
        } else if (type == VehicleType.TANK) {
            return instance.getTankGroundDamage();
        } else {
            throw new UnsupportedOperationException();
        }
    }

    // DEFENCE
    static double defenceOfType(VehicleType type, VehicleType opponentType) {
        return  defenceOfType(type, isFly(opponentType));
    }
    static double defenceOfType(VehicleType type, boolean isOpponentFly) {

        if (isOpponentFly) {
            return aerialDefenceOfType(type);
        } else {
            return groundDefenceOfType(type);
        }
    }
    static double aerialDefenceOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return instance.getArrvAerialDefence();
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterAerialDefence();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterAerialDefence();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvAerialDefence();
        } else if (type == VehicleType.TANK) {
            return instance.getTankAerialDefence();
        } else {
            throw new UnsupportedOperationException();
        }
    }
    static double groundDefenceOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return instance.getArrvGroundDefence();
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterGroundDefence();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterGroundDefence();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvGroundDefence();
        } else if (type == VehicleType.TANK) {
            return instance.getTankGroundDefence();
        } else {
            throw new UnsupportedOperationException();
        }
    }

    // VISION RANGE
    static double visionRangeOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return instance.getArrvVisionRange();
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterVisionRange();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterVisionRange();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvVisionRange();
        } else if (type == VehicleType.TANK) {
            return instance.getTankVisionRange();
        } else {
            throw new UnsupportedOperationException();
        }
    }
    static double getVisionRangeCoefficientOfType(VehicleType type, TerrainType terrainType, WeatherType weatherType) {

        if (type == VehicleType.ARRV) {
            return getTerrainCoefficientOfType(terrainType);
        } else if (type == VehicleType.FIGHTER) {
            return getWeatherCoefficientOfType(weatherType);
        } else if (type == VehicleType.HELICOPTER) {
            return getWeatherCoefficientOfType(weatherType);
        } else if (type == VehicleType.IFV) {
            return getTerrainCoefficientOfType(terrainType);
        } else if (type == VehicleType.TANK) {
            return getTerrainCoefficientOfType(terrainType);
        } else {
            throw new UnsupportedOperationException();
        }
    }
    static private double getTerrainCoefficientOfType(TerrainType type) {
        if (type == TerrainType.PLAIN) {
            return instance.getPlainTerrainVisionFactor();
        } else if (type == TerrainType.SWAMP) {
            return instance.getSwampTerrainVisionFactor();
        } else if (type == TerrainType.FOREST) {
            return instance.getForestTerrainVisionFactor();
        } else {
            throw new UnsupportedOperationException();
        }
    }
    static private double getWeatherCoefficientOfType(WeatherType type) {
        if (type == WeatherType.CLEAR) {
            return instance.getClearWeatherVisionFactor();
        } else if (type == WeatherType.CLOUD) {
            return instance.getCloudWeatherVisionFactor();
        } else if (type == WeatherType.RAIN) {
            return instance.getRainWeatherVisionFactor();
        } else {
            throw new UnsupportedOperationException();
        }
    }

    // SPEED
    static double speedOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return instance.getArrvSpeed();
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterSpeed();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterSpeed();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvSpeed();
        } else if (type == VehicleType.TANK) {
            return instance.getTankSpeed();
        } else {
            throw new UnsupportedOperationException();
        }
    }

    // PRODUCTION COST
    static int productionCostOfType(VehicleType type) {

        if (type == VehicleType.ARRV) {
            return instance.getArrvProductionCost();
        } else if (type == VehicleType.FIGHTER) {
            return instance.getFighterProductionCost();
        } else if (type == VehicleType.HELICOPTER) {
            return instance.getHelicopterProductionCost();
        } else if (type == VehicleType.IFV) {
            return instance.getIfvProductionCost();
        } else if (type == VehicleType.TANK) {
            return instance.getTankProductionCost();
        } else {
            throw new UnsupportedOperationException();
        }
    }

}
