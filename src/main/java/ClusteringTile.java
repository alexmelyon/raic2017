import java.util.ArrayList;
import java.util.List;

class ClusteringTile {

    private int row;
    private int col;
    private List<Integer> indices;
    private List<Integer> friendIndices;
    private int groupId = -1;

    ClusteringTile(int row, int col) {

        this.row = row;
        this.col = col;

        indices = new ArrayList<>(8);
        friendIndices = new ArrayList<>(4);
    }

    int getRow() {
        return row;
    }

    int getCol() {
        return col;
    }

    int getGroupId() {
        return groupId;
    }

    void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    void addIndex(int index) {
        indices.add(index);
    }

    void addFriend(int friendIndex) {
        friendIndices.add(friendIndex);
    }

    List<Integer> getIndices() {
        return indices;
    }

    List<Integer> getFriendIndices() {
        return friendIndices;
    }
}
