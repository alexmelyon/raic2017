class MoveMonitor {

    private State state;

    MoveMonitor() {
        state = State.FREE;
    }

    State getState() {
        return state;
    }

    void setState(State state) {
        this.state = state;
    }

    boolean stateIs(State otherState) {
        return state == otherState;
    }

    /**
     * Перечисленое с возможными состояниями монитора
     */
    enum State {

        /**
         * FREE - монитор свободен
         */
        FREE,

        /**
         * FORMATION - монитор захвачен контроллером построения (FormationHandler)
         */
        FORMATION,

        /**
         * BATTALION - монитор захвачен контроллером батальонов (BattalionsHandler)
         */
        BATTALION,

        /**
         * FIGHTERS - монитор захвачен контроллером истребителей (FightersHandler)
         */
        FIGHTERS
    }
}
