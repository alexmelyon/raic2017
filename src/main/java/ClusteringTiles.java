import model.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;

class ClusteringTiles {

    private static final int NUMBER_OF_TILES_IN_LINE = 128;
    private static final double MIN_FRIEND_DISTANCE = 9.0;
    private World world;
    private List<MyVehicle> vehicles;
    private Predicate<MyVehicle> filter;
    private ClusteringTile[][] clusteringTiles;
    private List<ClusteringTile> notEmptyTiles;
    private int numberOfGroups;

    // notEmptyTiles - initial capacity 1024
    ClusteringTiles(World world, List<MyVehicle> vehicles, Predicate<MyVehicle> filter) {

        this.world = world;
        this.vehicles = vehicles;
        this.filter = filter;

        clusteringTiles = new ClusteringTile[NUMBER_OF_TILES_IN_LINE][NUMBER_OF_TILES_IN_LINE];
        notEmptyTiles = new ArrayList<>(1024);

        for (int i = 0; i < NUMBER_OF_TILES_IN_LINE; i++) {
            for (int j = 0; j < NUMBER_OF_TILES_IN_LINE; j++) {
                clusteringTiles[i][j] = new ClusteringTile(i, j);
            }
        }
    }

    int getNumberOfGroups() {
        return numberOfGroups;
    }
    Iterator iterator() {
        return new Iterator();
    }


    void addVehicles() {

        for (int i = 0; i < vehicles.size(); i++) {

            if (!filter.test(vehicles.get(i))) {
                continue;
            }

            MyVehicle vehicle = vehicles.get(i);
            int row = (int) (vehicle.getY() * NUMBER_OF_TILES_IN_LINE / world.getHeight());
            int col = (int) (vehicle.getX() * NUMBER_OF_TILES_IN_LINE / world.getWidth());

            clusteringTiles[row][col].addIndex(i);
            if (clusteringTiles[row][col].getIndices().size() == 1 ) {
                notEmptyTiles.add(clusteringTiles[row][col]);
            }
        }
    }

    void checkNeighbors() {
        for (ClusteringTile tile : notEmptyTiles) {
            checkNeighborsFor(tile.getRow(), tile.getCol());
        }
    }
    private void checkNeighborsFor(int row, int col) {

        addUncheckedFriend(row, col, row, col + 1);
        addUncheckedFriend(row, col, row + 1, col - 1);
        addUncheckedFriend(row, col, row + 1, col);
        addUncheckedFriend(row, col, row + 1, col - 1);
    }
    private void addUncheckedFriend(int myRow, int myCol, int hisRow, int hisCol) {

        if ((myRow != hisRow || myCol != hisCol) &&
                checkRowAndCol(hisRow, hisCol) &&
                isFriends(clusteringTiles[myRow][myCol], clusteringTiles[hisRow][hisCol])) {

            clusteringTiles[myRow][myCol].addFriend(hisRow * NUMBER_OF_TILES_IN_LINE + hisCol);
            clusteringTiles[hisRow][hisCol].addFriend(myRow * NUMBER_OF_TILES_IN_LINE + myCol);
        }
    }
    private boolean isFriends(ClusteringTile my, ClusteringTile he) {

        for (Integer myIndex : my.getIndices()) {
            for (Integer hisIndex : he.getIndices()) {
                if (isFriends(myIndex, hisIndex)) {
                    return true;
                }
            }
        }

        return false;
    }
    private boolean isFriends(int i, int j) {

        Pos pi = new Pos(vehicles.get(i).getX(), vehicles.get(i).getY());
        Pos pj = new Pos(vehicles.get(j).getX(), vehicles.get(j).getY());

        return pi.distanceTo(pj) < MIN_FRIEND_DISTANCE;
    }
    private boolean checkRowAndCol(int row, int col) {
        return row >= 0 && row < NUMBER_OF_TILES_IN_LINE &&
                col >= 0 && col < NUMBER_OF_TILES_IN_LINE;
    }

    void combineGroups() {

        numberOfGroups = 0;
        for (ClusteringTile tile : notEmptyTiles) {
            if (tile.getGroupId() >= 0) {
                continue;
            }
            combineGroup(tile.getRow(), tile.getCol(), numberOfGroups);
            numberOfGroups++;
        }
    }
    private void combineGroup(int row, int col, int groupIndex) {

        Stack<ClusteringTile> groupStack = new Stack<>();
        groupStack.push(clusteringTiles[row][col]);

        while(!groupStack.empty()) {

            ClusteringTile current = groupStack.pop();
            if (current.getGroupId() >= 0) {
                continue;
            }
            current.setGroupId(groupIndex);

            for (Integer friend : current.getFriendIndices()) {

                int hisRow = friend / NUMBER_OF_TILES_IN_LINE;
                int hisCol = friend % NUMBER_OF_TILES_IN_LINE;

                if (clusteringTiles[hisRow][hisCol].getGroupId() < 0) {
                    groupStack.push(clusteringTiles[hisRow][hisCol]);
                }
            }
        }
    }

    class Iterator {

        private int currentPosition = -1;

        boolean hasNext() {
            return currentPosition + 1 < NUMBER_OF_TILES_IN_LINE * NUMBER_OF_TILES_IN_LINE;
        }

        ClusteringTile next() {

            currentPosition++;
            int currentRow = currentPosition / NUMBER_OF_TILES_IN_LINE;
            int currentCol = currentPosition % NUMBER_OF_TILES_IN_LINE;

            return clusteringTiles[currentRow][currentCol];
        }
    }
}
