class Tile implements Comparable<Tile> {

    private int row;
    private int col;
    double influence;

    Tile(int row, int col, double influence) {
        this.row = row;
        this.col = col;
        this.influence = influence;
    }

    Pos getCenter() {
        return new Pos(32 * col + 16, 32 * row + 16);
    }

    // сортировка по убыванию influence
    @Override
    public int compareTo(Tile tile) {
        return Double.compare(tile.influence, influence);
    }
}
