import model.FacilityType;
import model.Player;
import model.VehicleType;
import model.World;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

class BattalionsHandler extends AbstractHandler {

    // +-----------+
    // | КОНСТАНТЫ |
    // +-----------+

    private static final double NECESSARY_OF_DODGE_ENEMY_NUCLEAR = 2.0;

    // Выбор точек, в которых считаются потенциалы
    private static final int NUMBER_OF_DIRECTIONS_TO_ATTACK = 64;

    // количество точек на выбранном направлении и сами точки
    private static final double[] D_TICKS = { 10, 15, 20, 30, 50, 75, 105, 150, 210, 280, 360, 450, 550, 660, 780, 910, 1050, 1200 };

    // кол-во юнитов в новой группе
    private static final int DEFAULT_NUMBER_OF_UNITS_IN_NEW_BATTALION = 15;

    // допустимое кол-во юнитов, которые захватывают сооружение
    private static final int ALLOWABLE_NUMBERS_OF_INVADERS = 30;

    /**
     * UNIT_RADIUS чтобы не было деленья на 0 при вычислении потенциала
     *                                  ~ по сути это радиус юнита
     */
    private static final double UNIT_RADIUS = 2;

    // кол-во тиков, когда нельзя выделять батальон (так как скоро создастся новый юнит)
    private static final int TICK_COUNT_WHEN_CANT_SELECT = 10;

    /**
     * Для вычисления потенциала сооружения
     */
    private static final double FACILITY_POTENTIAL_VALUE_0 = 40;
    private static final double FACILITY_CENTER_RADIUS = 10;

    // +------+
    // | ПОЛЯ |
    // +------+

    private Random random;
    private Battalion current;
    private Battalion nuclearInteractBattalion;
    private int lastHandle;
    private boolean isSelected;
    private MoveMonitor.State monitorState;

    private Map<Integer, Integer> myPowerCoefficientId;
    private Map<Integer, Integer> opPowerCoefficientId;

    private double[][] powerCoefficients;
    private double[][] facilitiesCoefficients;

    // +--------+
    // | МЕТОДЫ |
    // +--------+

    BattalionsHandler(Army army, long randomSeed) {

        this.army = army;
        monitorState = MoveMonitor.State.BATTALION;

        if (random == null) {
            random = new Random(randomSeed);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    // Для визуализотора
    void setWorld(World world) {
        this.world = world;
    }

    // +--------------------+
    // | НЕОБХОДИМОСТЬ ХОДА |
    // +--------------------+

    @Override
    public double necessaryOfMove(World world, double resourcesToMakeMove) {

        nuclearInteractBattalion = null;
        double maxMoveNecessary = Double.MIN_VALUE;

        // необходимость задоджить ядерный взрыв противника
        if (isEnemyNuclearStrike(world)) {
            maxMoveNecessary = dodgeNuclearNecessary(world, army.getMyBattalions());
        }

        // необходимость собраться после доджа ядерного взрыва противника
        if (maxMoveNecessary == Double.MIN_VALUE &&
                !isEnemyNuclearStrike(world)) {
            maxMoveNecessary = groupNuclearNecessary(army.getMyBattalions());
        }

        // необходимость очередной сделать ход
        if (maxMoveNecessary == Double.MIN_VALUE) {
            maxMoveNecessary = resourcesToMakeMove > 3.1 ? 1.0 : 0;
        }

        return maxMoveNecessary;
    }

    // в качестве nuclearInteractBattalion берется самый скоростной
    private double dodgeNuclearNecessary(World world, List<? extends Battalion> structures) {

        double maxMoveNecessary = Double.MIN_VALUE;
        for (Battalion structure : structures) {
            double moveNecessary;
            if (inEnemyNuclearZone(world, structure) &&
                    structure.getTargetState() != Battalion.TargetState.DODGE_NUCLEAR &&
                    (moveNecessary = calcMoveNecessaryForNuclearVulnerableStructure(world, structure)) > maxMoveNecessary) {

                if (nuclearInteractBattalion == null || nuclearInteractBattalion.getSpeed() < structure.getSpeed()) {
                    nuclearInteractBattalion = structure;
                }
                maxMoveNecessary = moveNecessary;
            }
        }
        return maxMoveNecessary;
    }
    private boolean inEnemyNuclearZone(World world, Battalion structure) {
        return calcDistanceToEnemyNuclear(world, structure) < MyGame.instance.getTacticalNuclearStrikeRadius();
    }
    private double calcMoveNecessaryForNuclearVulnerableStructure(World world, Battalion structure) {

        double nuclearInfluenceCoefficient =
                1 - calcDistanceToEnemyNuclear(world, structure) / MyGame.instance.getTacticalNuclearStrikeRadius();

        return NECESSARY_OF_DODGE_ENEMY_NUCLEAR + nuclearInfluenceCoefficient;
    }
    private double calcDistanceToEnemyNuclear(World world, Battalion battalion) {

        Pos nuclearStrikePos = new Pos(
                world.getOpponentPlayer().getNextNuclearStrikeX(),
                world.getOpponentPlayer().getNextNuclearStrikeY());

        return battalion.distTo(nuclearStrikePos) - battalion.getMaxRadius();
    }
    private double groupNuclearNecessary(List<? extends Battalion> structures) {

        double maxMoveNecessary = Double.MIN_VALUE;
        for (Battalion structure : structures) {
            if (structure.getTargetState() == Battalion.TargetState.DODGE_NUCLEAR) {
                if (nuclearInteractBattalion == null || nuclearInteractBattalion.getSpeed() < structure.getSpeed()) {
                    nuclearInteractBattalion = structure;
                }
                maxMoveNecessary = NECESSARY_OF_DODGE_ENEMY_NUCLEAR;
            }
        }
        return maxMoveNecessary;
    }

    // +-------------------+
    // | ОСНОВНОЕ ДЕЙСТВИЕ |
    // +-------------------+

    @Override
    public void doAction(World world, MyMove move, double resourcesToMakeMove) {

        this.world = world;
        this.move = move;
        this.resourcesToMakeMove = resourcesToMakeMove;

        run();
        army.setCurrent(current);
    }

    private void run() {

        if (resourcesToMakeMove < 1) {
            return;
        }

        // Выбираем, чем ходить, если ещё не выбрали
        if (!isSelected) {
            setUpCurrentStructure();
            updateTarget();
        }

        // Делаем ход
        if (!isSelected) {
            selectBattalion();
            isSelected = true;
            moveMonitor.setState(monitorState);

        } else if (current.getUniqueId() < 0) {
            current.setUniqueId(army.nextBind());
            current.checkSelect();
            move.bind(current);

        } else {
            doMainMove();
            lastHandle = world.getTickIndex();
            System.out.println("[TICK INDEX = " + lastHandle + "] GROUP #" + current.getUniqueId());
            isSelected = false;
            moveMonitor.setState(MoveMonitor.State.FREE);
        }
    }

    // ВЫБОР ЧЕМ ХОДИТЬ
    private void setUpCurrentStructure() {

        // выбрать батальон, если противник использует ядерный взрыв или,
        //      который надо сгруппировать после ядерного взрыва противника,
        // либо батальон с максимальной разностью потенциалов
        current = nuclearInteractBattalion != null ?
                nuclearInteractBattalion :
                setUpBattalionWithMaxPotentialDifference();

        // если этот батальон уже выбран, то выделять его не надо
        if (army.getCurrent() != null && current.getUniqueId() == army.getCurrent().getUniqueId()) {
            isSelected = true;
        }
    }
    private Battalion setUpBattalionWithMaxPotentialDifference() {

        Battalion chosenBattalion = null;
        double maxPotentialDifference = Double.NEGATIVE_INFINITY;

        calcPowerCoefficients();
        calcFacilitiesCoefficients();

        for (Battalion battalion : army.getMyBattalions()) {

            if (isIgnored(battalion)) {
                continue;
            }

            double potentialDifference = calcPotentialDifferenceFor(battalion);

            if (battalion.getUniqueId() < 0 &&
                    battalion.getCount() < 30 &&
                    potentialDifference < 1e-3) {
                continue;
            }

            if (potentialDifference > maxPotentialDifference) {
                maxPotentialDifference = potentialDifference;
                chosenBattalion = battalion;
            }
        }

        return chosenBattalion == null ? current : chosenBattalion;
    }
    private boolean isIgnored(Battalion battalion) {

        boolean isScaling = battalion.getState() == Battalion.State.SCALE;
        boolean isNew = battalion.getUniqueId() < 0;
        boolean isLittle = battalion.getCount() < DEFAULT_NUMBER_OF_UNITS_IN_NEW_BATTALION;
        boolean isProductionSoon = false;

        if (isNew) {
            MyFacility battalionFacility = army.getFacilityById(-battalion.getUniqueId());
            isProductionSoon = battalionFacility.getPlayerId() == world.getMyPlayer().getId() &&
                    battalionFacility.getMaxProgress() - battalionFacility.getProgress() < TICK_COUNT_WHEN_CANT_SELECT;
        }

        return isScaling || (isNew && isLittle) || (isNew && isProductionSoon);
    }

    // ОБНОВЛЕНИЕ ЦЕЛЕЙ
    private void updateTarget() {

        if (isEnemyNuclearStrike(world) && inEnemyNuclearZone(world, current)) {
            setUpDodgeNuclearTarget();
            current.setTargetState(Battalion.TargetState.DODGE_NUCLEAR);

        } else if (!isEnemyNuclearStrike(world) && current.getTargetState() == Battalion.TargetState.DODGE_NUCLEAR) {
            setUpRegroupNuclearTarget();
            current.setTargetState(Battalion.TargetState.REGROUP_NUCLEAR);

        } else if (isDivided()) {
            setUpRegroupTarget();
            current.setTargetState(Battalion.TargetState.REGROUP);

        } else {
            setUpAttackTarget();
            current.setTargetState(Battalion.TargetState.ATTACK);
        }
    }
    private boolean isDivided() {
        return current.isDivided();
    }
    private void setUpDodgeNuclearTarget() {

        Player enemyPlayer = world.getOpponentPlayer();
        Pos nuclearStrike = new Pos(enemyPlayer.getNextNuclearStrikeX(), enemyPlayer.getNextNuclearStrikeY());
        current.setTarget(nuclearStrike);
    }
    private void setUpRegroupNuclearTarget() {
        current.setTarget(current.getCenter());
    }
    private void setUpRegroupTarget() {
        current.setTarget(current.getCenter());
    }
    private void setUpAttackTarget() {
        current.setTarget(current.getPreTarget());
    }

    // ВЫДЕЛЕНИЕ БАТАЛЬОНА
    private void selectBattalion() {

        if (current.getUniqueId() > 0) {
            move.select(current);
            return;
        }

        // todo v64 11 - это кол-ло юнитов в одной линии
        // -42, чтобы освободить id
        MyFacility facility = army.getFacilityById(-current.getUniqueId());
        current.setUniqueId(-42);

        double dy = UNIT_RADIUS + 3 * ((current.getCount() / 11) + 1);
        double left = facility.getLeft() - UNIT_RADIUS;
        double right = facility.getRight() + UNIT_RADIUS;
        double top = current.getCenter().y - dy;
        double bottom = current.getCenter().y + dy;

        Rectangle rect = new Rectangle(left, top, right, bottom);

        move.select(rect, facility.getVehicleType());

    }

    // СОВЕРШЕНИЕ ДЕЙСТВИЯ
    private void doMainMove() {

        Battalion.TargetState targetState = current.getTargetState();
        switch (targetState) {
            case ATTACK:
                doMoveAttack();
                break;
            case REGROUP:
                doMoveRegroup();
                break;
            case REGROUP_NUCLEAR:
                doMoveRegroupNuclear();
                break;
            case DODGE_NUCLEAR:
                doMoveDodge();
                break;
        }
    }
    private void doMoveAttack() {

        Vector2D direction = new Vector2D(current.getCenter(), current.getTarget());
        move.move(current, direction);
    }
    private void doMoveRegroup() {

        if (current.getState() == Battalion.State.ROTATE) {
            move.scale(current, current.getCenter(), 0.1);;

        } else {
            int signInt = random.nextInt(2);
            double sign = 2 * signInt - 1;
            double angle = sign * Math.PI;

            move.rotate(current, current.getTarget(), angle);
        }
    }
    private void doMoveRegroupNuclear() {
        move.scale(current, current.getCenter(), 0.1);;
    }
    private void doMoveDodge() {
        move.scale(current, current.getTarget(), 10);
    }

    // ВЫЧИСЛЕНИЕ РАЗНОСТИ ПОТЕНЦИАЛОВ
    private double calcPotentialDifferenceFor(Battalion battalion) {

        // Center Potential
        double CP = calcPotential(battalion, battalion.getCenter());
        // Relative Default Potential Difference
        double RDPD = calcRelativeDefaultPotentialDifference(battalion, CP);
        // Relative Potential Difference of Direction
        double maxRPDD = Double.NEGATIVE_INFINITY;
        Pos bestTarget = battalion.getTarget();

        for (int i = 0; i < NUMBER_OF_DIRECTIONS_TO_ATTACK; i++) {

            double directionAngle = -Math.PI + 2 * Math.PI * i / NUMBER_OF_DIRECTIONS_TO_ATTACK;
            Vector2D direction = new Vector2D(Math.cos(directionAngle), Math.sin(directionAngle));

            Pair<Double, Pos> currentEntry = calcRelativePotentialDifferenceOfDirection(battalion, CP, direction);
            double thisRPDD = currentEntry.first;
            Pos thisTarget = currentEntry.second;

            if (thisRPDD > maxRPDD) {
                maxRPDD = thisRPDD;
                bestTarget = thisTarget;
            }
        }

        battalion.setPreTarget(bestTarget);
        return maxRPDD - RDPD;
    }
    private double calcRelativeDefaultPotentialDifference(Battalion battalion, double CP) {

        // Relative Default Potential Difference
        double RDPD = Double.NEGATIVE_INFINITY;
        // From Center to Target
        Vector2D CT = new Vector2D(battalion.getCenter(), battalion.getTarget());

        // Случай, когда движение закончится меньше чем через 1 тик
        if (CT.length() < battalion.getSpeed()) {
            return 0;
        }

        double lastPD = Double.NEGATIVE_INFINITY;
        boolean stop = false;

        for (int i = 0; i < D_TICKS.length && !stop; i++) {

            double dT = D_TICKS[i];
            double currentDistance = dT * battalion.getSpeed();

            if (currentDistance >= CT.length()) {
                currentDistance = CT.length();
                dT = CT.length() / battalion.getSpeed();
                stop = true;
            }

            Pos currentPos = battalion.getCenter().shift(CT.norm(currentDistance));
            double currentPD = calcPotential(battalion, currentPos) - CP;
            double currentRPD = currentPD / dT;

            RDPD = Math.max(RDPD, currentRPD);
            if (currentPD < lastPD) {
                break;
            }
            lastPD = currentPD;
        }

        return RDPD;
    }
    private Pair<Double,Pos> calcRelativePotentialDifferenceOfDirection(Battalion battalion, double CP, Vector2D direction) {

        // Relative Potential Difference of Direction
        double RPDD = Double.NEGATIVE_INFINITY;
        Pos target = null;

        double lastPD = Double.NEGATIVE_INFINITY;

        for (double dT : D_TICKS) {

            double currentDistance = dT * battalion.getSpeed();
            Pos currentPos = battalion.getCenter().shift(direction.norm(currentDistance));

            // Нельзя выходить за границы области
            if (currentPos.x < 0 || currentPos.x > world.getWidth() ||
                    currentPos.y < 0 || currentPos.y > world.getWidth()) {
                break;
            }

            double currentPD = calcPotential(battalion, currentPos) - CP;
            double currentRPD = currentPD / dT;

            RPDD = Math.max(RPDD, currentRPD);
            if (currentPD < lastPD) {
                break;
            }
            lastPD = currentPD;
            target = currentPos;
        }

        return new Pair<>(RPDD, target);
    }

    // ВЫЧИСЛЕЕНИЕ ПОТЕНЦИАЛА
    private double calcPotential(Battalion battalion, Pos target) {

        double my = calcAllyPotential(battalion, target);
        double op = calcEnemyPotential(battalion, target);
        double facilities = calcFacilitiesPotential(battalion, target);
        double wall = calcWallPotential(battalion, target);

        double potential = my + op + facilities + wall;
        double sign = potential > 0 ? 1 : -1;

        return  sign * Math.max(0, 1 + Math.log10(Math.abs(potential)) / 5);
    }
    private double calcAllyPotential(Battalion battalion, Pos target) {

        double profit = 0;

        for (Battalion ally : army.getMyBattalions()) {
            profit += ally.calcPotential(1, battalion, target);
        }

        return profit;
    }
    private double calcEnemyPotential(Battalion battalion, Pos target) {

        double profit = 0;

        for (Battalion enemy : army.getOpBattalions()) {
            profit += calcEnemyPotential(battalion, enemy, target);
        }

        return profit;
    }
    private double calcFacilitiesPotential(Battalion battalion, Pos target) {

        double profit = 0;

        for (MyFacility facility : army.getFacilities()) {
            //profit += facility.calcPotential(world.getMyPlayer().getId(), battalion, target);
            profit += calcFacilityPotential(battalion, facility, target);
        }

        return profit;
    }
    private double calcWallPotential(Battalion battalion, Pos target) {

        double profit = 0;

        for (Wall wall : army.getWalls()) {
            profit += wall.calcPotential(world.getMyPlayer().getId(), battalion, target);
        }

        return profit;
    }

    // ВЫЧИСЛЕНИЕ ПОТЕНЦИАЛА ПРОТИВНИКА
    double calcEnemyPotential(Battalion me, Battalion op, Pos target) {

        Vector2D fromMeCenterToCenter = new Vector2D(target, op.getCenter());
        double lengthOfRadii = op.getCore().getRadiusForDirection(fromMeCenterToCenter)
                + me.getCore().getRadiusForDirection(fromMeCenterToCenter);
        double dist = fromMeCenterToCenter.length();

        double distanceToEnemy = Math.max(UNIT_RADIUS, dist);
        double distanceBetweenBattalions = Math.max(UNIT_RADIUS, distanceToEnemy - lengthOfRadii);

        double powerCoefficient = powerCoefficients
                [myPowerCoefficientId.get(me.getUniqueId())]
                [opPowerCoefficientId.get(op.getUniqueId())];
        double speedCoefficient = powerCoefficient > 0 ?
                me.getSpeed() / op.getSpeed() :
                op.getSpeed() / me.getSpeed();

        return powerCoefficient * speedCoefficient / (distanceBetweenBattalions * distanceToEnemy);

        /*
        double xInner = lengthOfRadii + INNER_RANGE;
        double xOuter = lengthOfRadii + OUTER_RANGE;

        double fx = Math.abs(powerCoefficient) / (distanceToEnemy * distanceToEnemy);
        double fxInner = Math.abs(powerCoefficient) / (xInner * xInner);
        double fxOuter = Math.abs(powerCoefficient) / (xOuter * xOuter);

        if (powerCoefficient > 0) {
            // врыв
            return fx;

        } else if (!me.isWeak()) {
            // движение около противника на небольшом радиусе
            return distanceToEnemy > lengthOfRadii + INNER_RANGE ? fx : -fx + 2 * fxInner;

        } else {
            // движдение около противника на большом радиусе
            return distanceToEnemy > lengthOfRadii + OUTER_RANGE ? fx : -fx + 2 * fxOuter;
        }*/
    }
    void calcPowerCoefficients() {

        myPowerCoefficientId = new HashMap<>();
        int myId = 0;
        for (Battalion me : army.getMyBattalions()) {
            myPowerCoefficientId.put(me.getUniqueId(), myId++);
        }

        opPowerCoefficientId = new HashMap<>();
        int opId = 0;
        for (Battalion op : army.getOpBattalions()) {
            opPowerCoefficientId.put(op.getUniqueId(), opId++);
        }

        powerCoefficients = new double[myId][opId];

        myId = 0;
        opId = 0;
        for (Battalion me : army.getMyBattalions()) {
            for (Battalion op : army.getOpBattalions()) {
                powerCoefficients[myId][opId] = calcPowerCoefficient(me, op);
                opId++;
            }
            myId++;
            opId = 0;
        }
    }
    // todo v60 тут есть, где развернуться
    private double calcPowerCoefficient(Battalion me, Battalion op) {

        double myHealth = me.getTotalDurability();
        double opHealth = op.getTotalDurability();
        double myDamage = calcDamage(me, op);
        double opDamage = calcDamage(op, me);

        int nIter = 0;
        int maxIter = 10;
        double myCurrentHealth = myHealth;
        double opCurrentHealth = opHealth;
        double myCurrentDamage = myDamage;
        double opCurrentDamage = opDamage;

        while (myCurrentHealth > 0 && opCurrentHealth > 0 && nIter++ < maxIter) {
            myCurrentHealth -= opCurrentDamage;
            opCurrentHealth -= myCurrentDamage;
            myCurrentDamage = myDamage * myCurrentHealth / myHealth;
            opCurrentDamage = opDamage * opCurrentHealth / opHealth;
        }

        double myRemaining = me.getCount() * Math.max(0, myCurrentHealth / myHealth);
        double opRemaining = op.getCount() * Math.max(0, opCurrentHealth / opHealth);
        double myLost = me.getCount() - myRemaining;
        double opLost = op.getCount() - opRemaining;

        return opLost - myLost;
    }
    private double calcDamage(Battalion attacker, Battalion defender) {

        int nf = attacker.getCountOfType(VehicleType.FIGHTER);
        int nh = attacker.getCountOfType(VehicleType.HELICOPTER);
        int na = attacker.getCountOfType(VehicleType.ARRV);
        int ni = attacker.getCountOfType(VehicleType.IFV);
        int nt = attacker.getCountOfType(VehicleType.TANK);
        int n0 = attacker.getCount();
        int nA = nf + nh;
        int nG = n0 - nA;
        double pA = ((double) nA) / n0;
        double pG = ((double) nG) / n0;

        int mf = defender.getCountOfType(VehicleType.FIGHTER);
        int mh = defender.getCountOfType(VehicleType.HELICOPTER);
        int ma = defender.getCountOfType(VehicleType.ARRV);
        int mi = defender.getCountOfType(VehicleType.IFV);
        int mt = defender.getCountOfType(VehicleType.TANK);
        int m0 = defender.getCount();
        int mA = mf + mh;
        int mG = m0 - mA;
        double qA = ((double) mA) / m0;
        double qG = ((double) mG) / m0;

        double res = 0;

        // pA & qA
        if (pA > 0 && qA > 0) {
            double avgDefAA = (mf * MyGame.aerialDefenceOfType(VehicleType.FIGHTER)
                    + mh * MyGame.aerialDefenceOfType(VehicleType.HELICOPTER))
                    / mA;
            double dmgfA = Math.max(0, nf * (MyGame.aerialDamageOfType(VehicleType.FIGHTER) - avgDefAA));
            double dmghA = Math.max(0, nh * (MyGame.aerialDamageOfType(VehicleType.HELICOPTER) - avgDefAA));
            res += pA * qA * (dmgfA + dmghA);
        }

        // pA & qG
        if (pA > 0 && qG > 0) {
            double avgDefAG = (mt * MyGame.aerialDefenceOfType(VehicleType.TANK)
                    + mi * MyGame.aerialDefenceOfType(VehicleType.IFV)
                    + ma * MyGame.aerialDefenceOfType(VehicleType.ARRV)
                    + ma * MyGame.instance.getArrvRepairSpeed() * MyGame.instance.getTankAttackCooldownTicks())
                    / mG;
            double dmgfG = Math.max(0, nf * (MyGame.groundDamageOfType(VehicleType.FIGHTER) - avgDefAG));
            double dmghG = Math.max(0, nh * (MyGame.groundDamageOfType(VehicleType.HELICOPTER) - avgDefAG));
            res += pA * qG * (dmgfG + dmghG);
        }

        // pG & qA
        if (pG > 0 && qA > 0) {
            double avgDefGA = (mf * MyGame.groundDefenceOfType(VehicleType.FIGHTER)
                    + mh * MyGame.groundDefenceOfType(VehicleType.HELICOPTER))
                    / mA;
            double dmgtA = Math.max(0, nt * (MyGame.aerialDamageOfType(VehicleType.TANK) - avgDefGA));
            double dmgiA = Math.max(0, ni * (MyGame.aerialDamageOfType(VehicleType.IFV) - avgDefGA));
            res += pG * qA * (dmgtA + dmgiA);
        }

        // pG & qG
        if (pG > 0 && qG > 0) {
            double avgDefGG = (mt * MyGame.groundDefenceOfType(VehicleType.TANK)
                    + mi * MyGame.groundDefenceOfType(VehicleType.IFV)
                    + ma * MyGame.groundDefenceOfType(VehicleType.ARRV)
                    + ma * MyGame.instance.getArrvRepairSpeed() * MyGame.instance.getTankAttackCooldownTicks())
                    / mG;
            double dmgtA = Math.max(0, nt * (MyGame.groundDamageOfType(VehicleType.TANK) - avgDefGG));
            double dmgiA = Math.max(0, ni * (MyGame.groundDamageOfType(VehicleType.IFV) - avgDefGG));
            res += pG * qG * (dmgtA + dmgiA);
        }

        return res;
    }

    // ВЫЧИСЛЕНИЕ ПОТЕНЦИАЛА СООРУЖЕНИЙ
    double calcFacilityPotential(Battalion battalion, MyFacility facility, Pos target) {

        double facilityCoefficient = facilitiesCoefficients
                [myPowerCoefficientId.get(battalion.getUniqueId())]
                [facility.getUniqueId()];

        double distanceToFacility = Math.max(FACILITY_CENTER_RADIUS, target.distanceTo(facility.getCenter()));
        double distanceToFacilityWithPenalty = distanceToFacility + FACILITY_POTENTIAL_VALUE_0;

        return facilityCoefficient / distanceToFacilityWithPenalty;
    }
    void calcFacilitiesCoefficients() {

        // кажется facility.uniqueId идет с 1 до N
        facilitiesCoefficients = new double[army.getMyBattalions().size()][army.getFacilities().size() + 1];

        int batId = 0;
        for (Battalion battalion : army.getMyBattalions()) {
            for (MyFacility facility: army.getFacilities()) {
                facilitiesCoefficients[batId][facility.getUniqueId()] = calcFacilityCoefficient(battalion, facility);
            }
            batId++;
        }
    }
    // TODO v85 коэффициенты пока прямо здесь
    private double calcFacilityCoefficient(Battalion battalion, MyFacility facility) {

        int battalionGroundCount = battalion.getCountOfType(VehicleType.IFV) +
                battalion.getCountOfType(VehicleType.TANK) + battalion.getCountOfType(VehicleType.ARRV);
        int battalionAntiGroundCount = battalion.getCountOfType(VehicleType.HELICOPTER) +
                battalion.getCountOfType(VehicleType.TANK);

        double distToFacilityWithPenalty = 100 + battalion.distTo(facility.getCenter());

        double FACILITY_FIGHTERS_ATTRACTIVE = 0.1;
        double FACILITY_HELICOPTERS_ATTRACTIVE = 35 / distToFacilityWithPenalty;
        double FACILITY_PROTECT = 0.1 * battalionAntiGroundCount;
        double FACILITY_SEIZURE_VF = 0.3 * battalionGroundCount;
        double FACILITY_SEIZURE_CC = 0.25 * battalionGroundCount;
        double FACILITY_SEIZURE = facility.getType() == FacilityType.VEHICLE_FACTORY ?
                FACILITY_SEIZURE_VF :
                FACILITY_SEIZURE_CC;

        // Истребители и сооружения противника
        if (battalion.getCountOfType(VehicleType.FIGHTER) > 0 &&
                facility.getPlayerId() == world.getOpponentPlayer().getId() &&
                facility.getType() == FacilityType.VEHICLE_FACTORY &&
                MyGame.isFly(facility.getVehicleType())) {
            return FACILITY_FIGHTERS_ATTRACTIVE;
        }
        // Вертолеты и сооружения противника
        if (battalion.getCountOfType(VehicleType.HELICOPTER) > 0 &&
                facility.getPlayerId() == world.getOpponentPlayer().getId() &&
                facility.getType() == FacilityType.VEHICLE_FACTORY &&
                !MyGame.isFly(facility.getVehicleType())) {
            return FACILITY_HELICOPTERS_ATTRACTIVE;
        }
        // Защита своих сооружений от захвата (для вертолетов и танков)
        if (facility.getPlayerId() == world.getMyPlayer().getId() &&
                facility.isAttacked() &&
                (battalion.getCountOfType(VehicleType.TANK) > 0 ||
                battalion.getCountOfType(VehicleType.HELICOPTER) > 0)) {
            return FACILITY_PROTECT;
        }
        // v95 ARRV не должны захватывать VF противника (кроме случая, когда они ропизводят FIGHTER)
        if (battalion.getCountOfType(VehicleType.ARRV) > 0 &&
                facility.getPlayerId() == world.getOpponentPlayer().getId() &&
                facility.getType() == FacilityType.VEHICLE_FACTORY &&
                facility.getVehicleType() != VehicleType.FIGHTER) {
            return 0;
        }
        // захват сооружений (для наземной техники)
        if (facility.getPlayerId() != world.getMyPlayer().getId() &&
                battalion.isGround()) {
            return FACILITY_SEIZURE * calcFacilityPositiveConflictCoefficient(battalion, facility);
        }

        return 0;
    }
    private double calcFacilityPositiveConflictCoefficient(Battalion battalion, MyFacility facility) {

        int battalionGroundCount = battalion.getCountOfType(VehicleType.IFV) +
                battalion.getCountOfType(VehicleType.TANK) + battalion.getCountOfType(VehicleType.ARRV);
        double capturePoints = facility.getPlayerId() == world.getOpponentPlayer().getId() ? 200 : 100;
        double tCapture = capturePoints / (battalionGroundCount * MyGame.instance.getFacilityCapturePointsPerVehiclePerTick());

        double distBetweenCenters = battalion.getCenter().distanceTo(facility.getCenter());
        double t1 = distBetweenCenters / battalion.getSpeed();

        // если не успеем даже доехать до сооружения, то и не едем
        if (t1 + tCapture + world.getTickIndex() > world.getTickCount()) {
            return 0;
        }

        int numberOfInvaders = 0;
        double EPSILON = 1e-5;

        for (Battalion ally : army.getMyBattalions()) {
            if (ally.getUniqueId() == battalion.getUniqueId() || ally.isAerial()) {
                continue;
            }
            Vector2D allyShift = new Vector2D(ally.getCenter(), ally.getTarget());
            double tAlly = allyShift.length() / ally.getSpeed();
            Vector2D shift = allyShift.length() < EPSILON ? allyShift : allyShift.norm(ally.getSpeed() * Math.min(tAlly, t1));
            Pos movedTo = ally.getCenter().shift(shift);
            if (facility.contains(movedTo)) {
                numberOfInvaders += ally.getCount();
            }
        }

        return numberOfInvaders < ALLOWABLE_NUMBERS_OF_INVADERS ? 1 : 0;
    }
}
